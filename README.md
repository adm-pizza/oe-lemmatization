# OE Lemmatization

An attempt at AI-based lemmatization of Old English, or at least grouping words with common lemmas.


## Overview

The study of Old English is more of an art than a science, where translator interpretation and 
understanding of context plays a big role. 

From the little I've researched, there's very little use of machine learning tools in the study of it.
One big thing that makes it difficult to analyze programmatically is the variability of word spelling - 
it predates standardized spelling, and as such words can be read and pronounced the same with drastically
different spellings. Proper lemmatization can solve part of this problem, resolving words with ambiguous 
spellings to a common lemma (or word root).


## Project goal

This project aims to create a neural network for lemmatization of Old English. The ideal end goal
is an engine that takes an Old English word as input, and outputs a lemma - but what I believe would be 
simpler and more reliable from a machine learning standpoint is for it to take two Old English words
as inputs and output a _probability_ of them having a common lemma. Additionally, the latter comparative
engine could help to create the former transformative one.


## Training data

As with most ML projects, its success hinges on the data available. To train the engine, we need a dataset
of word pairs with common lemmas. These inputs correspond to a high probability output for backpropagation.
For negative low-probability training data we can take two random words from the entire corpus of Old English.

There's a ton of well-annotated Old English data available on [Wiktionary](https://en.wiktionary.org/wiki/Category:Old_English_language),
and while I still need to check with some OE experts what they think of it, a very easily usable form of it 
is provided by Tatu Ylonen [here](https://kaikki.org/dictionary/Old%20English/index.html). The machine-readable
nature of the data makes this an ideal starting point.

## Technical plan

- Use [word embeddings](https://www.tensorflow.org/text/guide/word_embeddings) for input
- Common lemma pair approach: Use [Tensorflow Similarity](https://github.com/tensorflow/similarity) (probably with [Triplet Loss](https://en.wikipedia.org/wiki/Triplet_loss))
- Lemmatizer approach: Use some other [Tensorflow Text](https://www.tensorflow.org/text) approach - likely something simpler, maybe an RNN

Considering variability of spelling with OE, I believe the most reliable way to go about this is the former common pair approach.


## Potential issues

### Overfitting to Levenshtein distance
Word pairs with common lemmas likely have high [Levenshtein distance](https://en.wikipedia.org/wiki/Levenshtein_distance),
and this strikes me as something that could result in overfitting. The words `hlaford`, `hlavord`, and 
`hlafordes` share lemmas, with Levenshtein distances of 1 to 4. However, `lagu` and `wæccend` (different 
lemmas, chosen randomly) have a much higher distance of 7 between them. 

To adjust for this, the ideal training data might emphasize word pairs with different lemmas that have low
Levenshtein distance.


## Roadmap

- [x] Find usable dictionary data
- [ ] Preprocess data into lemma groups
- [ ] Write script to pull random lemma pairs from groups, to create training data
- [ ] Train Tensorflow engine
