#!/usr/bin/env python3

import json
from unidecode import unidecode
import unicodedata

encoding = [["þ","z"],["ð","q"],["æ","j"]]


def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')
                  
def encode(s):
    for e in encoding:
        s = s.replace(e[0], e[1])
    return s
    
def decode(s):
    for e in encoding:
        s = s.replace(e[1], e[0])
    return s

class OEP:
    lgroups = {}
    # TODO: finish this parser, to make adding words easier
    # Look into the "senses" field a bit more; seems there's alternative 
    # and inflected words there we need to make sense of (see weotuma vs wituma)
    #@staticmethod
    #def add


link=""
#link = "https://kaikki.org/dictionary/Old%20English/kaikki.org-dictionary-OldEnglish.json"

filename = "dictionary-oe.json"
#filename = "subdic.json"

# all pos types:
# name, noun, pron, adv, conj, prefix, num, verb, adj, prep, article, suffix,
# intj, character, det, particle, proverb, phrase, prep_phrase

pos_types = ["noun", "pron", "adj", "adv", "article", "conj", "verb" ]
form_tags_to_exclude = ["table-tags", "inflection-template", "class"]

if link:
    filename = link.split("/")[-1]

with open(filename) as f:
    data = f.read()

entries = data.strip().split("\n")

lgroups = {}
pos = {}

for e in entries:
    j = json.loads(e)
    word = j["word"]
    #print(word,"-",strip_accents(encode(j["word"])),"-",j["pos"])
    
    if j["pos"] not in pos_types:
        continue

    

    if "form-of" in j.keys():
        original = j["form-of"]["word"]
        if original in lgroups:
            lgroups[original] += [word]
        else:
            lgroups[original] = [word]
        continue
        
    if "alt-of" in j.keys():
        original = j["alt-of"]["word"]
        if original in lgroups:
            lgroups[original] += [word]
        else:
            lgroups[original] = [word]
        continue

    if "senses" in j.keys
            
    if "form-of" not in j.keys():
        # we got a root word, add a new entry

        if word in lgroups:
            #wtf? why is it here
            lgroups[word] += [word]
            
        else:
            lgroups[word] = [word]
        
        if "forms" in j.keys():
            forms = []
            for f in j["forms"]:
                if f["tags"][0] not in form_tags_to_exclude and f["form"] != "-":
                    forms += [f["form"]] if f["form"] not in forms else []

            lgroups[word] += forms

for g in lgroups:
    print(", ".join(lgroups[g]))
        
print("found",len(lgroups),"groups")    
print("parts of speech:",", ".join(pos.keys()))    


output = json.dumps(lgroups, ensure_ascii=False, sort_keys=True, indent=4)
with open("groups.json", "w") as f:
    f.write(output)
    

